-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-09-29 20:36:27.316

-- tables
-- Table: Clans
CREATE TABLE Clans (
    id int NOT NULL,
    name varchar(100) NOT NULL,
    logo varchar(500) NOT NULL,
    ranking int NOT NULL,
    CONSTRAINT Clans_pk PRIMARY KEY (id)
);

-- Table: Game
CREATE TABLE Game (
    id int NOT NULL,
    name varchar(150) NOT NULL,
    url varchar(300) NOT NULL,
    CONSTRAINT Game_pk PRIMARY KEY (id)
);

-- Table: Gameperlancenter
CREATE TABLE Gameperlancenter (
    id int NOT NULL,
    id_lancenter int NOT NULL,
    id_game int NOT NULL,
    CONSTRAINT Gameperlancenter_pk PRIMARY KEY (id)
);

-- Table: Gamers
CREATE TABLE Gamers (
    id int NOT NULL,
    username varchar(50) NOT NULL,
    email varchar(50) NOT NULL,
    phone varchar(15) NOT NULL,
    password varchar(50) NOT NULL,
    FirstName varchar(50) NOT NULL,
    LastName varchar(50) NOT NULL,
    CONSTRAINT Gamers_pk PRIMARY KEY (id)
);

-- Table: Lancenter
CREATE TABLE Lancenter (
    id int NOT NULL,
    name varchar(75) NOT NULL,
    address varchar(100) NOT NULL,
    latitude varchar(50) NOT NULL,
    longitude varchar(50) NOT NULL,
    email varchar(50) NOT NULL,
    password varchar(50) NOT NULL,
    CONSTRAINT Lancenter_pk PRIMARY KEY (id)
);

-- Table: Members
CREATE TABLE Members (
    id int NOT NULL,
    id_user int NOT NULL,
    id_team int NOT NULL,
    CONSTRAINT Members_pk PRIMARY KEY (id)
);

-- Table: Participants
CREATE TABLE Participants (
    id int NOT NULL,
    id_team int NOT NULL,
    id_tournament int NOT NULL,
    CONSTRAINT Participants_pk PRIMARY KEY (id)
);

-- Table: Publication
CREATE TABLE Publication (
    id int NOT NULL,
    id_lancenter int NOT NULL,
    content varchar(300) NOT NULL,
    date date NOT NULL,
    id_user int NOT NULL,
    type int NOT NULL,
    CONSTRAINT Publication_pk PRIMARY KEY (id)
);

-- Table: Reservation
CREATE TABLE Reservation (
    id int NOT NULL,
    id_lancenter int NOT NULL,
    message varchar(300) NOT NULL,
    state int NOT NULL,
    id_user int NOT NULL,
    time_init time NOT NULL,
    time_end time NOT NULL,
    CONSTRAINT Reservation_pk PRIMARY KEY (id)
);

-- Table: Tournament
CREATE TABLE Tournament (
    id int NOT NULL,
    date date NOT NULL,
    id_game int NOT NULL,
    id_lancenter int NOT NULL,
    id_winner int NOT NULL,
    CONSTRAINT Tournament_pk PRIMARY KEY (id)
);

-- Table: Versus
CREATE TABLE Versus (
    id_versus int NOT NULL,
    id_team1 int NOT NULL,
    id_team2 int NOT NULL,
    date date NOT NULL,
    id_game int NOT NULL,
    id_winner int NOT NULL,
    CONSTRAINT Versus_pk PRIMARY KEY (id_versus)
);

-- foreign keys
-- Reference: Gameperlancenter_Game (table: Gameperlancenter)
ALTER TABLE Gameperlancenter ADD CONSTRAINT Gameperlancenter_Game FOREIGN KEY Gameperlancenter_Game (id_game)
    REFERENCES Game (id);

-- Reference: Gameperlancenter_Lancenter (table: Gameperlancenter)
ALTER TABLE Gameperlancenter ADD CONSTRAINT Gameperlancenter_Lancenter FOREIGN KEY Gameperlancenter_Lancenter (id_lancenter)
    REFERENCES Lancenter (id);

-- Reference: Members_Teams (table: Members)
ALTER TABLE Members ADD CONSTRAINT Members_Teams FOREIGN KEY Members_Teams (id_team)
    REFERENCES Clans (id);

-- Reference: Members_Usuario (table: Members)
ALTER TABLE Members ADD CONSTRAINT Members_Usuario FOREIGN KEY Members_Usuario (id_user)
    REFERENCES Gamers (id);

-- Reference: Participants_Teams (table: Participants)
ALTER TABLE Participants ADD CONSTRAINT Participants_Teams FOREIGN KEY Participants_Teams (id_team)
    REFERENCES Clans (id);

-- Reference: Participants_Tournament (table: Participants)
ALTER TABLE Participants ADD CONSTRAINT Participants_Tournament FOREIGN KEY Participants_Tournament (id_tournament)
    REFERENCES Tournament (id);

-- Reference: Publication_Lancenter (table: Publication)
ALTER TABLE Publication ADD CONSTRAINT Publication_Lancenter FOREIGN KEY Publication_Lancenter (id_lancenter)
    REFERENCES Lancenter (id);

-- Reference: Publication_Usuario (table: Publication)
ALTER TABLE Publication ADD CONSTRAINT Publication_Usuario FOREIGN KEY Publication_Usuario (id_user)
    REFERENCES Gamers (id);

-- Reference: Reservas_Lancenter (table: Reservation)
ALTER TABLE Reservation ADD CONSTRAINT Reservas_Lancenter FOREIGN KEY Reservas_Lancenter (id_lancenter)
    REFERENCES Lancenter (id);

-- Reference: Reservation_User (table: Reservation)
ALTER TABLE Reservation ADD CONSTRAINT Reservation_User FOREIGN KEY Reservation_User (id_user)
    REFERENCES Gamers (id);

-- Reference: Tournament_Game (table: Tournament)
ALTER TABLE Tournament ADD CONSTRAINT Tournament_Game FOREIGN KEY Tournament_Game (id_game)
    REFERENCES Game (id);

-- Reference: Tournament_Lancenter (table: Tournament)
ALTER TABLE Tournament ADD CONSTRAINT Tournament_Lancenter FOREIGN KEY Tournament_Lancenter (id_lancenter)
    REFERENCES Lancenter (id);

-- Reference: Versus_Game (table: Versus)
ALTER TABLE Versus ADD CONSTRAINT Versus_Game FOREIGN KEY Versus_Game (id_game)
    REFERENCES Game (id);

-- Reference: Versus_Teams (table: Versus)
ALTER TABLE Versus ADD CONSTRAINT Versus_Teams FOREIGN KEY Versus_Teams (id_team1)
    REFERENCES Clans (id);

-- End of file.

